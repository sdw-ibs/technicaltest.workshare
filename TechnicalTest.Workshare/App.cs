﻿using System;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using DllImportAttribute = System.Runtime.InteropServices.DllImportAttribute;

namespace TechnicalTest.Workshare
{
	internal static class App
	{
		#region Log
		//To save time I'm using pure strings. Normally, I would pin down the types of these fields! 
		private sealed class Log
		{
			public string Date { get; set; }
			public string Time { get; set; }
			public string ServerIP { get; set; }
			public string Method { get; set; }
			public string UriStem { get; set; }
			public string UriQuery { get; set; }
			public string Port { get; set; }
			public string UserName { get; set; }
			public string ClientIP { get; set; }
			public string UserAgent { get; set; }
			public string Referer { get; set; }
			public string Status { get; set; }
			public string SubStatus { get; set; }
			public string Win32Status { get; set; }
			public string TimeTaken { get; set; }
		}
		#endregion

		#region Helpers
		[DllImport("kernel32.dll")]
		private static extern IntPtr GetConsoleWindow();

		[DllImport("user32.dll")]
		private static extern bool ShowWindow(IntPtr wnd, int cmdShow);

		private static void MaximizeConsole() =>
			ShowWindow(GetConsoleWindow(), 3);

		private static void WriteTitle()
		{
			var title = $"{nameof(TechnicalTest)}.{nameof(Workshare)}";
			var border = new string('-', title.Length);

			Console.WriteLine("{0}{1}{2}{1}{0}", border, Environment.NewLine, title);
		}
		#endregion

		#region Logic
		private const string TestLogFileName = @"..\..\..\_TestLog\log.txt";

		private static Log GetLog(string logLine)
		{
			if (string.IsNullOrWhiteSpace(logLine))
				return null;

			var split = logLine.Split(' ');

			if (split.Length != 15)
				return null;

			return new Log
			{
				Date = split[0],
				Time = split[1],
				ServerIP = split[2],
				Method = split[3],
				UriStem = split[4],
				UriQuery = split[5],
				Port = split[6],
				UserName = split[7],
				ClientIP = split[8],
				UserAgent = split[9],
				Referer = split[10],
				Status = split[11],
				SubStatus = split[12],
				Win32Status = split[13],
				TimeTaken = split[14]
			};
		}

		private static IEnumerable<Log> GetLogs(string logFileName)
		{
			if (!File.Exists(logFileName))
				throw new FileNotFoundException(nameof(logFileName), logFileName);

			return File
				.ReadAllLines(logFileName)
				.Where(x => !x.StartsWith("#"))
				.Select(GetLog)
				.Where(x => x != null);
		}

		private static IEnumerable<string> GetLogStats(IEnumerable<Log> logs, Func<Log, string> keySelector) =>
			logs.GroupBy(keySelector)
			  .OrderByDescending(x => x.Count())
			  .Select(x => $"{x.Key} ({x.Count()})");
		#endregion

		#region Exception Handling
		static App() => AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
		{
			Console.WriteLine((Exception)args.ExceptionObject);
			Environment.Exit(-1);
		};
		#endregion

		private static void Main()
		{
			MaximizeConsole();
			WriteTitle();

			var logFileName = TestLogFileName;
			var logs = GetLogs(logFileName);

			Console.WriteLine("\nLog Filename : {0}", Path.GetFileName(logFileName));
			Console.WriteLine("Logs Found : {0}\n", logs.Count());

			Console.WriteLine("Most Used ClientIP : {0}", GetLogStats(logs, x => x.ClientIP).First());
			Console.WriteLine("Most Used UriStem : {0}\n", GetLogStats(logs, x => x.UriStem).First());
			Console.WriteLine("Status Code Frequency : {0}\n", JsonConvert.SerializeObject(GetLogStats(logs, x => x.Status), Formatting.Indented));

			Console.WriteLine("Logged total time taken : {0}\n", logs.Sum(x => int.Parse(x.TimeTaken)));
		}
	}
}